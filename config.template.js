var config = {};

config.repo_user = 'username';
config.repo_pass = 'password';

config.repo_url = 'https://api.bitbucket.org/1.0/repositories/';

config.key = 'your_bitbucket_oauth2_key';
config.secret = 'your_bitbucket_oauth2_secret';

module.exports = config;