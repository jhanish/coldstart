const fs = require('fs');

const config = require('./config');

const request = require('request');
const sanitize = require('sanitize-filename');
const yargs = require('yargs')

  .command('create [name]', 'Create new project', (yargs) => {
      yargs
        .positional('name', {
            describe: 'name of project / dir / repo',
            demand: true
        })
  })
    .argv;

switch(yargs._[0]) {
    case 'create':
        console.log('Create command called.');
        create();
        break;
    default:
        console.log('Unknown command: ' + yargs._[0]);
}

function create() {
    console.log('Inside create function.');

    var dir = sanitize(yargs.name);
    console.log(dir);
    console.log('Creating directory: ' + dir);

    if(!fs.existsSync(dir)) {
        //fs.mkdirSync(dir);
    }

    console.log('Creating repository');
    createRepo();
}

function createRepo() {

    console.log("In createRepo()");

    request({
        method: 'POST',
        uri: config.repo_url,
        auth: {
            username: config.repo_user,
            password: config.repo_pass
        },
        form: {
            name: sanitize(yargs.name)
        }

    }, (error, response, body) => {
        
        console.log('Call to create repo finished.');
        console.log(error);
        console.log("\n\n");
        console.log(response);
        console.log("\n\n");
        console.log(body);
    });
    
}

console.log(yargs.name);
console.log(yargs._[0]);

//curl --user user:password https://api.bitbucket.org/1.0/repositories/ --data name=node-weather

